package com.exo2.Exercice2.service;

import com.exo2.Exercice2.dto.EtudiantDto;
import com.exo2.Exercice2.entity.Etudiant;
import com.exo2.Exercice2.mapper.EtudiantMapper;
import com.exo2.Exercice2.repository.EtudiantRepository;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EtudiantService {

    private final EtudiantRepository etudiantRepository;
    private final EtudiantMapper etudiantMapper;

    @Cacheable(value = "etudiants")
    public Page<EtudiantDto> findAll(Pageable pageable) {
        Page<Etudiant> etudiantsPage = etudiantRepository.findAll(pageable);
        return etudiantsPage.map(etudiantMapper::toDto);
    }

    @Cacheable(value = "etudiants", key = "#id")
    public EtudiantDto findById(Long id) {
        return etudiantMapper.toDto(etudiantRepository.findById(id).orElse(null));
    }

    @Cacheable(value = "etudiants", key = "#nom + '_' + #prenom")
    public EtudiantDto findOneByNomAndPrenom(String nom, String prenom) {
        return etudiantMapper.toDto(etudiantRepository.findOneEtudiantByNomAndPrenom(nom, prenom).orElse(null));
    }

    @CacheEvict(value = "etudiants", allEntries = true)
    public EtudiantDto save(EtudiantDto etudiantDto) {
        Etudiant etudiant = etudiantRepository.save(etudiantMapper.toEntity(etudiantDto));
        return etudiantMapper.toDto(etudiant);
    }

    @CacheEvict(value = "etudiants", key = "#id")
    public EtudiantDto update(Long id, EtudiantDto etudiantDto) {
        return etudiantRepository.findById(id)
                .map(existingEtudiant -> {
                    Etudiant etudiant = etudiantMapper.toEntity(etudiantDto);
                    etudiant.setId(id);
                    if (existingEtudiant.getEcole() != null) {
                        etudiant.setEcole(existingEtudiant.getEcole());
                    }
                    if (existingEtudiant.getProjets() != null && !existingEtudiant.getProjets().isEmpty()) {
                        etudiant.setProjets(existingEtudiant.getProjets());
                    }
                    return etudiantMapper.toDto(etudiantRepository.save(etudiant));
                })
                .orElse(null);
    }

    @CacheEvict(value = "etudiants", key = "#id")
    public void delete(Long id) {
        etudiantRepository.deleteById(id);
    }
}
